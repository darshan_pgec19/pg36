#include<stdio.h>
int main()
{
 struct DOB
 {
  int days;
  int months;
  int year;
 };
 struct student
 {
  int rno;
  char name[20];
  float fees;
  struct DOB d;
 }s;
 printf("\n Enter the roll number:");
 scanf("%d",&s.rno);
 printf("\n Enter student name:");
 scanf("%s",s.name);
 printf("\n Enter the fees:");
 scanf("%f",&s.fees);
 printf("\n Enter the DOB:");
 scanf("%d %d %d",&s.d.days,&s.d.months,&s.d.year);
 printf("\n Student details:");
 printf("\n Roll No:%d",s.rno);
 printf("\n Name:%s",s.name);
 printf("\n Fees:%f",s.fees);
 printf("\n DOB:%d-%d-%d",s.d.days,s.d.months,s.d.year);
 return 0;
} 