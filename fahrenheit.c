#include <stdio.h>
float input()
{
    float c;
    printf("enter the temperature in degree celsius \n");
    scanf("%f",&c);
    return c;
}
float compute(float c)
{
    float f;
    f=(9/5)*c+32;
    return f;
}
void output(float f)
{
    printf("the temperature in fahrenheit scale is %f \n",f);
}
int main()
{
    float c,f;
    c=input();
    f=compute(c);
    output(f);
    return 0;
}
