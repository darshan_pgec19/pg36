#include <stdio.h>
float input()
{
    float f;
    printf("enter the temperature in fahrenheit scale \n");
    scanf("%f",&f);
    return f;
}
float compute(float f)
{
    float c;
    c=((f-32)*(0.556));
    return c;
}
void output(float c)
{
    printf("the temperature in degree celsius is %f \n",c);
}
int main()
{
    float c,f;
    f=input();
    c=compute(f);
    output(c);
    return 0;
}
