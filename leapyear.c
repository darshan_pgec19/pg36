#include <stdio.h>
int input()
{
    int x;
    printf("enter the year \n");
    scanf("%d",&x);
    return x;
}
int compute(int x)
{
    if((x%4==0&&x%100!=0)||(x%400==0&&x%100==0))
    return 1;
  else
      return 0;
}

void output(int y)
{
    if(y==1)
    printf("the entered year is a leap year\n" );
    else
        printf("the year is not a leap year \n ");
}
int main()
{
    int x,y;
    x=input();
    y=compute(x);
    output(y);
    return 0;
}