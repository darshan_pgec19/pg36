#include<stdio.h>
int main()
{
 struct DOR
 {
  int d,m,y;
 };
 struct library_book
 {
  char name[20];
  int accession_no;
  char author[20];
  int edition;
  struct DOR date;
 }s;
 printf("\nenter the name of the book:");
 scanf("%s",s.name);
 printf("\nenter the accession no:");
 scanf("%d",&s.accession_no);
 printf("\nenter the author of the book:");
 scanf("%s",s.author);
 printf("\nenter the edition of the book:");
 scanf("%d",&s.edition);
 printf("\nenter the date of return:");
 scanf("%d %d %d",&s.date.d,&s.date.m,&s.date.y);
 printf("\n BOOK DETAILS");
 printf("\nName:%s",s.name);
 printf("\nAccession_no:%d",s.accession_no);
 printf("\nAuthor:%s",s.author);
 printf("\nEdition:%d",s.edition);
 printf("\nDOR:%d-%d-%d",s.date.d,s.date.m,s.date.y);
 return 0;
}